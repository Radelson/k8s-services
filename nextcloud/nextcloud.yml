# Source: nextcloud/charts/mariadb/templates/master-configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nextcloud-mariadb-master
  labels:
    app: "mariadb"
    component: "master"
    chart: "mariadb-7.1.0"
    release: "nextcloud"
    heritage: "Helm"
data:
  my.cnf: |-
    [mysqld]
    skip-name-resolve
    explicit_defaults_for_timestamp
    basedir=/opt/bitnami/mariadb
    plugin_dir=/opt/bitnami/mariadb/plugin
    port=3306
    socket=/opt/bitnami/mariadb/tmp/mysql.sock
    tmpdir=/opt/bitnami/mariadb/tmp
    max_allowed_packet=16M
    bind-address=0.0.0.0
    pid-file=/opt/bitnami/mariadb/tmp/mysqld.pid
    log-error=/opt/bitnami/mariadb/logs/mysqld.log
    character-set-server=UTF8
    collation-server=utf8_general_ci
    
    [client]
    port=3306
    socket=/opt/bitnami/mariadb/tmp/mysql.sock
    default-character-set=UTF8
    plugin_dir=/opt/bitnami/mariadb/plugin
    
    [manager]
    port=3306
    socket=/opt/bitnami/mariadb/tmp/mysql.sock
    pid-file=/opt/bitnami/mariadb/tmp/mysqld.pid
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nextcloud-nextcloud-code
  labels:
    app.kubernetes.io/name: nextcloud
    helm.sh/chart: nextcloud-1.9.1
    app.kubernetes.io/instance: nextcloud
    app.kubernetes.io/managed-by: Helm
spec:
  accessModes:
    - "ReadWriteOnce"
  resources:
    requests:
      storage: "10Gi"
---
# Source: nextcloud/charts/mariadb/templates/master-svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: nextcloud-mariadb
  labels:
    app: "mariadb"
    component: "master"
    chart: "mariadb-7.1.0"
    release: "nextcloud"
    heritage: "Helm"
spec:
  type: ClusterIP
  ports:
  - name: mysql
    port: 3306
    targetPort: mysql
  selector:
    app: "mariadb"
    component: "master"
    release: "nextcloud"
---
# Source: nextcloud/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nextcloud
  labels:
    app.kubernetes.io/name: nextcloud
    helm.sh/chart: nextcloud-1.9.1
    app.kubernetes.io/instance: nextcloud
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
  - port: 8086
    targetPort: http
    protocol: TCP
    name: http
  selector:
    app.kubernetes.io/name: nextcloud
---
# Source: nextcloud/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nextcloud
  labels:
    app.kubernetes.io/name: nextcloud
    helm.sh/chart: nextcloud-1.9.1
    app.kubernetes.io/instance: nextcloud
    app.kubernetes.io/managed-by: Helm
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app.kubernetes.io/name: nextcloud
      app.kubernetes.io/instance: nextcloud
  template:
    metadata:
      labels:
        app.kubernetes.io/name: nextcloud
        app.kubernetes.io/instance: nextcloud
    spec:
      containers:
      - name: nextcloud
        image: "nextcloud:20.0.2-apache"
        imagePullPolicy: IfNotPresent
        env:
        - name: MYSQL_HOST
          value: nextcloud-mariadb
        - name: UMASK
          value: "007"
        - name: MYSQL_DATABASE
          value: "nextcloud"
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              name: nextcloud-db
              key: db-username
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: nextcloud-db
              key: db-password
        - name: NEXTCLOUD_ADMIN_USER
          valueFrom:
            secretKeyRef:
              name: nextcloud
              key: nextcloud-username
        - name: NEXTCLOUD_ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              name: nextcloud
              key: nextcloud-password
        - name: NEXTCLOUD_TRUSTED_DOMAINS
          value: cloud.adel.cool
        - name: NEXTCLOUD_DATA_DIR
          value: "/var/www/html/data"
        ports:
        - name: http
          containerPort: 80
          protocol: TCP
        livenessProbe:
          httpGet:
            path: /status.php
            port: http
            httpHeaders:
            - name: Host
              value: "cloud.adel.cool"
          initialDelaySeconds: 6000
          periodSeconds: 15
          timeoutSeconds: 5
          successThreshold: 1
          failureThreshold: 3
        resources:
          {}
        volumeMounts:
        - name: nextcloud-code
          mountPath: /var/www/
          subPath: root
        - name: nextcloud-code
          mountPath: /var/www/html
          subPath: html
        - name: nextcloud-data
          mountPath: /var/www/html/data
          subPath: data
        - name: alicia-share
          mountPath: /var/www/alicia-share
        - name: music
          mountPath: /var/www/music
        - name: music-mess
          mountPath: /var/www/music-mess
        - name: nextcloud-code
          mountPath: /var/www/html/config
          subPath: config
        - name: nextcloud-code
          mountPath: /var/www/html/custom_apps
          subPath: custom_apps
        - name: nextcloud-code
          mountPath: /var/www/tmp
          subPath: tmp
        - name: nextcloud-code
          mountPath: /var/www/html/themes
          subPath: themes
      volumes:
      - name: nextcloud-data
        nfs:
            path: /srv/nfs/nextcloud-data
            server: 192.168.0.100
      - name: music
        nfs:
            path: /srv/nfs/music
            server: 192.168.0.100
      - name: music-mess
        nfs:
            path: /srv/nfs/music-mess
            server: 192.168.0.100
      - name: alicia-share
        nfs:
            path: /srv/nfs/alicia-share
            server: 192.168.0.100
      - name: nextcloud-code
        persistentVolumeClaim:
          claimName: nextcloud-nextcloud-code
      # Will mount configuration files as www-data (id: 33) for nextcloud
      securityContext:
        fsGroup: 33
---
# Source: nextcloud/charts/mariadb/templates/master-statefulset.yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: nextcloud-mariadb-master
  labels:
    app: mariadb
    chart: mariadb-7.1.0
    release: nextcloud
    heritage: Helm
    component: master
spec:
  selector:
    matchLabels:
      app: mariadb
      release: nextcloud
      component: master
  serviceName: nextcloud-mariadb-master
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: mariadb
        chart: mariadb-7.1.0
        release: nextcloud
        component: master
    spec:
      serviceAccountName: default
      securityContext:
        fsGroup: 1001
        runAsUser: 1001
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 1
              podAffinityTerm:
                topologyKey: kubernetes.io/hostname
                labelSelector:
                  matchLabels:
                    app: mariadb
                    release: nextcloud      
      containers:
        - name: "mariadb"
          image: docker.io/bitnami/mariadb:10.3.20-debian-9-r0
          imagePullPolicy: "IfNotPresent"
          env:
            - name: MARIADB_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: nextcloud-mariadb
                  key: mariadb-root-password
            - name: MARIADB_USER
              value: "nextcloud"
            - name: MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: nextcloud-mariadb
                  key: mariadb-password
            - name: MARIADB_DATABASE
              value: "nextcloud"
          ports:
            - name: mysql
              containerPort: 3306
          livenessProbe:
            exec:
              command: ["sh", "-c", "exec mysqladmin status -uroot -p$MARIADB_ROOT_PASSWORD"]
            initialDelaySeconds: 120
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            exec:
              command: ["sh", "-c", "exec mysqladmin status -uroot -p$MARIADB_ROOT_PASSWORD"]
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          volumeMounts:
            - name: data
              mountPath: /bitnami/mariadb
            - name: config
              mountPath: /opt/bitnami/mariadb/conf/my.cnf
              subPath: my.cnf
      volumes:
        - name: config
          configMap:
            name: nextcloud-mariadb-master
  volumeClaimTemplates:
    - metadata:
        name: data
        labels:
          app: "mariadb"
          component: "master"
          release: "nextcloud"
          heritage: "Helm"
      spec:
        accessModes:
          - "ReadWriteOnce"
        resources:
          requests:
            storage: "8Gi"
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: nextcloud-db-backup
spec:
  # Backup the database every day at 2AM
  schedule: "0 2 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: nextcloud-db-backup
            image: mariadb:10.5.8-focal
            command: ["/bin/sh"]
            args: ["-c", 'mysqldump --all-databases -h"$MYSQL_HOST" -uroot -p"$MYSQL_PWD" > /var/backups/nextcloud-db-backup.sql']
            env:
            - name: MYSQL_PWD
              valueFrom:
                secretKeyRef:
                  name: nextcloud-mariadb
                  key: mariadb-root-password
            - name: MYSQL_HOST
              value: nextcloud-mariadb
            volumeMounts:
            - mountPath: /var/backups
              name: nextcloud-db-backup
          restartPolicy: Never
          volumes:
          - name: nextcloud-db-backup
            nfs:
                path: /srv/nfs/backups
                server: 192.168.0.100