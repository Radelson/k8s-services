# Home server

Those are the manifests used for my homeserver.
More infos can be scrapped from adel.cool

Thanks to https://github.com/erebe/personal-server for some nice inspirations (if not outright plagiarism).
https://cwienczek.com/2020/06/simple-backup-of-postgres-database-in-kubernetes/

## Not automatable things

Port forwarding box

## Things to automate

- linux install
- nfs exports
- packages install
- network config

- convert dpedr to use nfs for files
- convert "classical nfs" to use the nfs server https://westzq1.github.io/k8s/2019/06/28/nfs-server-on-K8S.html

## How to generate a PGP key + save it

* ```gpg --full-generate-key```

* ```gpg --fingerprint```

* ```gpg --armor --export ****************************** > pub-gpg-nginx.asc ```

* ```gpg --armor --export-secret-key ******************** > sec-gpg-nginx.asc```

* Save it in bitwarden