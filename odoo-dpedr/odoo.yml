---
# Source: odoo/charts/postgresql/templates/svc-headless.yaml
apiVersion: v1
kind: Service
metadata:
  name: odoo-server-postgresql-headless
  labels:
    app.kubernetes.io/name: postgresql
    helm.sh/chart: postgresql-10.1.3
    app.kubernetes.io/instance: odoo-server
    app.kubernetes.io/managed-by: Helm
    # Use this annotation in addition to the actual publishNotReadyAddresses
    # field below because the annotation will stop being respected soon but the
    # field is broken in some versions of Kubernetes:
    # https://github.com/kubernetes/kubernetes/issues/58662
    service.alpha.kubernetes.io/tolerate-unready-endpoints: "true"
spec:
  type: ClusterIP
  clusterIP: None
  # We want all pods in the StatefulSet to have their addresses published for
  # the sake of the other Postgresql pods even before they're ready, since they
  # have to be able to talk to each other in order to become ready.
  publishNotReadyAddresses: true
  ports:
    - name: tcp-postgresql
      port: 5432
      targetPort: tcp-postgresql
  selector:
    app.kubernetes.io/name: postgresql
    app.kubernetes.io/instance: odoo-server
---
# Source: odoo/charts/postgresql/templates/svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: odoo-server-postgresql
  labels:
    app.kubernetes.io/name: postgresql
    helm.sh/chart: postgresql-10.1.3
    app.kubernetes.io/instance: odoo-server
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
    - name: tcp-postgresql
      port: 5432
      targetPort: tcp-postgresql
  selector:
    app.kubernetes.io/name: postgresql
    app.kubernetes.io/instance: odoo-server
    role: primary
---
# Source: odoo/templates/svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: odoo-server
  labels:
    app.kubernetes.io/name: odoo
    helm.sh/chart: odoo-17.0.2
    app.kubernetes.io/instance: odoo-server
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
  - port: 8080
    targetPort: http
    protocol: TCP
    name: http
  selector:
    app.kubernetes.io/name: odoo
    app.kubernetes.io/instance: odoo-server
---
# Source: odoo/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: odoo-server
  labels:
    app.kubernetes.io/name: odoo
    helm.sh/chart: odoo-17.0.2
    app.kubernetes.io/instance: odoo-server
    app.kubernetes.io/managed-by: Helm
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: odoo
      app.kubernetes.io/instance: odoo-server
  template:
    metadata:
      labels:
        app.kubernetes.io/name: odoo
        helm.sh/chart: odoo-17.0.2
        app.kubernetes.io/instance: odoo-server
        app.kubernetes.io/managed-by: Helm
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/name: odoo
                    app.kubernetes.io/instance: odoo-server
                namespaces:
                  - default
                topologyKey: kubernetes.io/hostname
              weight: 1
      securityContext:
        fsGroup: 1001
      containers:
        - name: odoo-server
          image: docker.io/bitnami/odoo:14.0.20210110-debian-10-r10
          imagePullPolicy: "IfNotPresent"
          env:
            - name: POSTGRESQL_HOST
              value: "odoo-server-postgresql"
            - name: WITHOUT_DEMO
              value: 'all'
            - name: POSTGRESQL_USER
              value: "postgres"
            - name: POSTGRESQL_PORT_NUMBER
              value: "5432"
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: odoo-server-postgresql
                  key: "postgresql-password"
            - name: ODOO_EMAIL
              value: "me@adel.cool"
            - name: ODOO_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: odoo-server
                  key: odoo-password
          ports:
            - name: http
              containerPort: 8069
          livenessProbe:
            httpGet:
              path: /
              port: http
            initialDelaySeconds: 600
            periodSeconds: 30
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 6
          readinessProbe:
            httpGet:
              path: /
              port: http
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 6
          resources:
            requests:
              cpu: 300m
              memory: 512Mi
          volumeMounts:
            - name: odoo-data
              mountPath: /bitnami/odoo
      volumes:
        - name: odoo-data
          nfs:
              path: /srv/nfs/odoo-data
              server: 192.168.0.100
---
# Source: odoo/charts/postgresql/templates/statefulset.yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: odoo-server-postgresql
  labels:
    app.kubernetes.io/name: postgresql
    helm.sh/chart: postgresql-10.1.3
    app.kubernetes.io/instance: odoo-server
    app.kubernetes.io/managed-by: Helm
spec:
  serviceName: odoo-server-postgresql-headless
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app.kubernetes.io/name: postgresql
      app.kubernetes.io/instance: odoo-server
      role: primary
  template:
    metadata:
      name: odoo-server-postgresql
      labels:
        app.kubernetes.io/name: postgresql
        helm.sh/chart: postgresql-10.1.3
        app.kubernetes.io/instance: odoo-server
        app.kubernetes.io/managed-by: Helm
        role: primary
    spec:      
      securityContext:
        fsGroup: 1001
      containers:
        - name: odoo-server-postgresql
          image: docker.io/bitnami/postgresql:11.10.0-debian-10-r24
          imagePullPolicy: "IfNotPresent"
          resources:
            requests:
              cpu: 250m
              memory: 256Mi
          securityContext:
            runAsUser: 1001
          env:
            - name: BITNAMI_DEBUG
              value: "false"
            - name: POSTGRESQL_PORT_NUMBER
              value: "5432"
            - name: POSTGRESQL_VOLUME_DIR
              value: "/bitnami/postgresql"
            - name: PGDATA
              value: "/bitnami/postgresql/data"
            - name: POSTGRES_USER
              value: "postgres"
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: odoo-server-postgresql
                  key: postgresql-password
            - name: POSTGRESQL_ENABLE_LDAP
              value: "no"
            - name: POSTGRESQL_ENABLE_TLS
              value: "no"
            - name: POSTGRESQL_LOG_HOSTNAME
              value: "false"
            - name: POSTGRESQL_LOG_CONNECTIONS
              value: "false"
            - name: POSTGRESQL_LOG_DISCONNECTIONS
              value: "false"
            - name: POSTGRESQL_PGAUDIT_LOG_CATALOG
              value: "off"
            - name: POSTGRESQL_CLIENT_MIN_MESSAGES
              value: "error"
            - name: POSTGRESQL_SHARED_PRELOAD_LIBRARIES
              value: "pgaudit"
          ports:
            - name: tcp-postgresql
              containerPort: 5432
          livenessProbe:
            exec:
              command:
                - /bin/sh
                - -c
                - exec pg_isready -U "postgres" -h 127.0.0.1 -p 5432
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 6
          readinessProbe:
            exec:
              command:
                - /bin/sh
                - -c
                - -e
                - |
                  exec pg_isready -U "postgres" -h 127.0.0.1 -p 5432
                  [ -f /opt/bitnami/postgresql/tmp/.initialized ] || [ -f /bitnami/postgresql/.initialized ]
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 6
          volumeMounts:
            - name: dshm
              mountPath: /dev/shm
            - name: data
              mountPath: /bitnami/postgresql
      volumes:
        - name: dshm
          emptyDir:
            medium: Memory
            sizeLimit: 1Gi
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes:
          - "ReadWriteOnce"
        resources:
          requests:
            storage: "8Gi"
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: odoo-dpedr-db-backup
spec:
  # Backup the database every day at 2AM
  schedule: "0 2 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: odoo-dpedr-db-backup
            image: postgres:11-alpine
            env:
            - name: POSTGRES_HOST_AUTH_METHOD
              value: trust
            - name: POSTGRESQL_HOST
              value: "odoo-server-postgresql"
            - name: PGPASSWORD
              valueFrom:
                secretKeyRef:
                  name: odoo-server-postgresql
                  key: postgresql-password
            command: ["/bin/sh"]
            args: ["-c", 'pg_dumpall -c -U postgres -h $POSTGRESQL_HOST > /var/backups/odoo-dpedr-db-backup.sql']
            volumeMounts:
            - mountPath: /var/backups
              name: odoo-dpedr-db-backup
          restartPolicy: Never
          volumes:
          - name: odoo-dpedr-db-backup
            nfs:
                path: /srv/nfs/backups
                server: 192.168.0.100