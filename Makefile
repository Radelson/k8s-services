HOST='root@erebe.eu'
RASPBERRY='pi@10.200.200.2'

.PHONY: install dashboard ssh package nextcloud odoo

## Should install the commited ssh key to the local env.
install:
	sops -d --extract '["public_key"]' --output ~/.ssh/erebe_eu.pub secrets/ssh.yml
	sops -d --extract '["private_key"]' --output ~/.ssh/erebe_eu secrets/ssh.yml
	chmod 600 ~/.ssh/erebe_eu*
	grep -q erebe.eu ~/.ssh/config > /dev/null 2>&1 || cat config/ssh_client_config >> ~/.ssh/config
	mkdir ~/.kube || exit 0
	sops -d --output ~/.kube/config secrets/kubernetes-config.yml

## Should update the distant ssh config with the commited one.
ssh:
	ssh ${HOST} "cat /etc/ssh/sshd_config" | diff  - config/sshd_config \
		|| (scp config/sshd_config ${HOST}:/etc/ssh/sshd_config && ssh ${HOST} systemctl restart sshd)

## Should setup the distant env with default configs.
package:

## Should apply the local manifest.
nextcloud:
	sops -d --output secrets_decrypted/nextcloud.yml secrets/nextcloud.yml
	kubectl apply -f secrets_decrypted/nextcloud.yml
	kubectl apply -f nextcloud/nextcloud.yml

## Should apply the local manifest.
odoo:
	sops -d --output secrets_decrypted/odoo.yml secrets/odoo.yml
	kubectl apply -f secrets_decrypted/odoo.yml
	kubectl apply -f odoo-dpedr/odoo.yml

## Should apply the K8S dashboard manifests, start the proxy and output the connection token
dashboard:
	@kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
	@kubectl apply -f dashboard/dashboard-admin.yaml
	@echo -e "\n\nGetting the admin token...\n"
	@kubectl get secret -n kubernetes-dashboard $$(kubectl get serviceaccount admin-user -n kubernetes-dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
	@echo -e "\n\nStarting the proxy...\n"
	@echo -e "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login /n"
	@kubectl proxy
