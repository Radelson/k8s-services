#!/bin/bash
sudo mount -U 7ae793ca-bce7-4fd0-8c30-be57ae4539dc /mnt/backup
sudo btrfs subvolume snapshot -r /mnt/disks/raid1/nfs /mnt/disks/raid1/snapshots/nfs/initial-snapshot
sudo btrfs send /mnt/disks/raid1/snapshots/initial-snapshot | btrfs receive /mnt/backup/raid1-snapshots/initial-snapshot